<?php
  
  /**
   * @file
   * 
   * All theme functions for the eID module
   */

  /**
   * Content of the login block
   */
  function theme_eid_login_block($url) {
    return '<a href="'. $url .'"><img src="/'. drupal_get_path('module', 'eid') .'/img/logo_eid.png" /></a>';
  }