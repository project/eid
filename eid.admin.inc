<?php
  
  /**
   * @file
   * 
   * eID authentication admin functions
   */

  /**
   * Admin settings form
   */
  function eid_admin_settings() {
    $form = array();
    
    $form['eid_proxy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Proxy settings'),
      '#description' => t('eID authentication is handled by a webserver with the Belgium SSL certificate installed.'),
    );
    
    $form['eid_proxy']['eid_proxy_url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('The URL from the script where eID data can be retreived'),
      '#default_value' => variable_get('eid_proxy_url', 'eid.coworks.net/eid.php'),
      '#field_prefix' => 'https://',
    );
    
    $form['eid_proxy']['eid_proxy_uselogin'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use login'),
      '#description' => t('Check this box if you want to use a login on the reverse proxy. (Staging server needs this)'),
      '#default_value' => variable_get('eid_proxy_uselogin', 0),
    );
    
    $form['eid_proxy']['eid_proxy_login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#description' => t('The login you received from us'),
      '#default_value' => variable_get('eid_proxy_login', ''),
    );
    
    return system_settings_form($form);
  }