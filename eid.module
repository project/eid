<?php
  
  /**
   * @file
   * 
   * eID authentication module
   */

  /**
   * Implementation of hook_menu
   */
  function eid_menu() {
    $items = array();
    
    $items['admin/settings/eid'] = array(
      'title' => 'eID settings',
      'description' => 'Change settings of eID authentication.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('eid_admin_settings'),
      'file' => 'eid.admin.inc',
      'access arguments' => array('administer eid'),
    );
    
    $items['eid/response'] = array(
      'title' => 'eId response URL',
      'page callback' => 'eid_response',
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    );
    
    $items['eid/email'] = array(
      'title' => 'eId e-mail',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('eid_email_form'),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    );
    
    return $items;
  }
  
  /**
   * Implementation of hook_perm
   */
  function eid_perm() {
    return array('administer eid');
  }
    
  /**
   * Implementation of hook_block
   */
  function eid_block($op = 'list', $delta = 0, $edit = array()) {
    if (variable_get('eid_proxy_url', 'eid.coworks.net/eid_new.php') != '' && variable_get('eid_proxy_login', '') != '') {
      if ($op == 'list') {
        $blocks = array();
        $blocks[0] = array('info' => t('eID login'));
        return $blocks;
      }
      
      if ($op == 'view') {
        switch ($delta) {
          case 0:
            $block = array('subject' => t('Login with your eID'), 'content' => eid_block_login());
            break;
        }
        
        return $block;
      }
    }
  }
  
  /**
   * Creates the login block
   */
  function eid_block_login() {
    //Get the admin variable
    $url = variable_get('eid_proxy_url', 'eid.coworks.net/eid_new.php');
    
    //Build the query
    $query = array();
    $query[] = 'token='. drupal_get_token();
    
    if (variable_get('eid_proxy_uselogin', 0) != 0 && variable_get('eid_proxy_login', '') != '') {
      $query[] = 'login='. urlencode(variable_get('eid_proxy_login', ''));
    }
    $query = implode('&', $query);
    
    $url = 'https://'. $url .'?'. $query;
    
    return theme('eid_login_block', $url);
  }
  
  /**
   * Implementation of hook_theme
   */
  function eid_theme() {
    return array(
      'eid_login_block' => array(
        'arguments' => array(
          'url' => NULL,
        ),
        'file' => 'eid.theme.inc',
      ),
    );
  }
  
  /**
   * Callback for the response URL
   */
  function eid_response() {
    global $user;
    
    if (!isset($_GET['error'])) {
      $token = $_GET['token'];
      
      if (drupal_valid_token($token)) {
        //GET THE VARS
        $serialnr = urldecode($_GET['serialnr']);
        
        //CHECK IF THERE IS ALREADY A USER WITH THIS SERIALNR
        if ($uid = _eid_check_serial($serialnr)) {
          //LOAD THE USER
          $user = user_load(array('uid' => $uid));
        }
        else {
          //CREATE THE USER
          //REDIRECT USER TO THE EMAIL FORM, AFTER THAT USER IS CREATED AND LOGGED IN
          unset($_GET['q']);
          $query = array();
          
          foreach ($_GET as $var => $val) {
            $query[] = $var  . '=' . $val;
          }
          
          drupal_goto('eid/email', implode('&', $query));
        }
        
        drupal_goto();
      }
    } 
    else {
      switch ($_GET['error']) {
        case 1:
          $error = 'No token given';
          break;
          
        case 2:
          $error = 'No login given or login isn\'t in the correct format';
          break;
          
        case 3:
          $error = 'Login was incorrect';
          break;
          
        case 4:
          $error = 'eID authentication not accessed from the right place';
          break;
      }
      
      drupal_set_message(t($error), 'error');
      drupal_goto();
    }
  }
  
  /**
   * Internal function to check the serialnr
   */
  function _eid_check_serial($serialnr) {
    $result = db_query('SELECT COUNT(uid) FROM {eid_login} WHERE rrk = "%s"', $serialnr);

    if (db_result($result)) {
      $result = db_fetch_object(db_query('SELECT uid FROM {eid_login} WHERE rrk = "%s"', $serialnr));
      return $result->uid;
    } 
    else {
      return FALSE;
    }
  }
  
  /**
   * Implementation of hook_user
   */
  function eid_user($op, &$edit, &$account, $category = NULL) {
    if ($op == 'delete') {
      db_query('DELETE FROM {eid_login} WHERE uid = %d', $account->uid);
    }
    
    if ($op == 'view') {
      $accountinfo = _eid_get_accountinfo();
      
      $account->content['eid']['#type'] = 'user_profile_category';
      $account->content['eid']['#title'] = t('eID');
      
      $account->content['eid']['name']['#type'] = 'user_profile_item';
      $account->content['eid']['name']['#title'] = t('Name');
      $account->content['eid']['name']['#value'] = $accountinfo->lastname;
      
      $account->content['eid']['firstname']['#type'] = 'user_profile_item';
      $account->content['eid']['firstname']['#title'] = t('Firstname');
      $account->content['eid']['firstname']['#value'] = $accountinfo->firstname;
      
      $account->content['eid']['serialnr']['#type'] = 'user_profile_item';
      $account->content['eid']['serialnr']['#title'] = t('Serial Number');
      $account->content['eid']['serialnr']['#value'] = $accountinfo->rrk;
    }
  }
  
  /**
   * User has to give up his mailaddress the first time he logs in with eID
   */
  function eid_email_form() {
    $form = array();
    
    $form['eid_email'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#required' => TRUE,
    );
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    
    return $form;
  }
  
  /**
   * Validate function for email form
   */
  function eid_email_form_validate($form, &$form_state) {
    if (!_eid_validate_email($form_state['values']['eid_email'])) {
      form_set_error('eid_email', t('This is not a valid e-mailaddress'));
    }
  }
  
  /**
   * Internal function to validate a emailaddress
   */
  function _eid_validate_email($email) {
    if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
      return FALSE;
    }
    
    return TRUE;
  }
  
  /**
   * Submit function for email form
   */
  function eid_email_form_submit($form, &$form_state) {
    if (drupal_valid_token($_GET['token'])) {
      global $user;
      
      $firstname = urldecode($_GET['firstname']);
      $lastname = urldecode($_GET['lastname']);
      $serialnr = urldecode($_GET['serialnr']);
    
      $account = new stdClass();
    $account->name = $firstname .' '. $lastname;
    $account->status = 1;
    $account->mail = $form_state['values']['eid_email'];
  
    $user = user_save($account, (array)$account);

    //INSERT RECORD TO DB
    db_query('INSERT INTO {eid_login} (uid, firstname, lastname, rrk) VALUES (%d, "%s", "%s", "%s")', $user->uid, $firstname, $lastname, $serialnr);
  
    drupal_goto();
    } 
    else {
      drupal_set_message(t('We detected an invalid token. This attempt will be logged.'), 'error');
    }
  }
  
  /**
   * Internal to get the account info of this user
   */
  function _eid_get_accountinfo() {
    global $user;
    
    $accountinfo = db_fetch_object(db_query('SELECT firstname, lastname, rrk FROM {eid_login} WHERE uid = %d', $user->uid));
    
    return $accountinfo;
  }